webpackJsonp([0],{

/***/ 109:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 109;

/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/add-expense-popover/add-expense-popover.module": [
		151
	],
	"../pages/expense-report/expense-report.module": [
		153
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 150;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddExpensePopoverPageModule", function() { return AddExpensePopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_expense_popover__ = __webpack_require__(152);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddExpensePopoverPageModule = /** @class */ (function () {
    function AddExpensePopoverPageModule() {
    }
    AddExpensePopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_expense_popover__["a" /* AddExpensePopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__add_expense_popover__["a" /* AddExpensePopoverPage */]),
            ],
        })
    ], AddExpensePopoverPageModule);
    return AddExpensePopoverPageModule;
}());

//# sourceMappingURL=add-expense-popover.module.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddExpensePopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_expenses_expenses__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddExpensePopoverPage = /** @class */ (function () {
    function AddExpensePopoverPage(navCtrl, alertCtrl, expenses) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.expenses = expenses;
        this.selected = '';
        this.title = '';
        this.amount = '';
        this.increment = '';
    }
    AddExpensePopoverPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    AddExpensePopoverPage.prototype.addAmount = function () {
        if (!this.amount || !this.title || this.title === '' || this.amount === '' || this.amount === '0' || this.amount === '0.00')
            return;
        this.amount = this.amount.replace(',', '');
        if (!Number(this.amount)) {
            this.alertCtrl.create({
                title: 'Oops',
                message: 'Amount must be numbers only!',
                buttons: ['OK']
            }).present();
            return;
        }
        if (this.selected === '' || this.increment === '') {
            this.alertCtrl.create({
                title: 'Oops',
                message: 'Please select a frequency and increment!',
                buttons: ['OK']
            }).present();
            return;
        }
        this.expenses.addNewExpense(this.title, Number(this.amount), this.selected, this.increment);
        this.navCtrl.pop();
    };
    AddExpensePopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-expense-popover',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/finance_calculator/src/pages/add-expense-popover/add-expense-popover.html"*/'<ion-header>\n  <ion-row>\n    <ion-col text-right col-12 tappable (click)="close()">\n      <ion-icon name="add" item-end class="cross-icon"></ion-icon>\n    </ion-col>\n  </ion-row>\n  <ion-grid class="chip-grid">\n    <ion-row>\n      <ion-col col-auto text-center>\n        <button class="chip" [ngClass]="selected === \'one-time\' ? \'chip-selected\' : \'\'" (click)="selected = \'one-time\'">\n          One-Time\n        </button>\n      </ion-col>\n      <ion-col col-auto text-center>\n        <button class="chip" [ngClass]="selected === \'daily\' ? \'chip-selected\' : \'\'" (click)="selected = \'daily\'">\n          Daily\n        </button>\n      </ion-col>\n      <ion-col col-auto text-center>\n        <button class="chip" [ngClass]="selected === \'weekly\' ? \'chip-selected\' : \'\'" (click)="selected = \'weekly\'">\n          Weekly\n        </button>\n      </ion-col>\n      <ion-col col-auto text-center>\n        <button class="chip" [ngClass]="selected === \'monthly\' ? \'chip-selected\' : \'\'" (click)="selected = \'monthly\'">\n          Monthly\n        </button>\n      </ion-col>\n      <ion-col col-auto text-center>\n        <button class="chip" [ngClass]="selected === \'annual\' ? \'chip-selected\' : \'\'" (click)="selected = \'annual\'">\n          Annual\n        </button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6 text-center>\n        <button class="increment" [ngClass]="increment === \'deposit\' ? \'chip-selected\' : \'\'" (click)="increment = \'deposit\'">\n          Deposit\n        </button>\n      </ion-col>\n      <ion-col col-6 text-center>\n        <button class="increment" [ngClass]="increment === \'subtract\' ? \'chip-selected\' : \'\'" (click)="increment = \'subtract\'">\n          Subtract\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-header>\n\n\n<ion-content class="no-scroll">\n  <ion-list no-lines no-border text-center>\n\n    <ion-item>\n      <ion-label>Title</ion-label>\n      <ion-input type="text" [(ngModel)]="title"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>Amount</ion-label>\n      <ion-input type="tel" [(ngModel)]="amount"></ion-input>\n      <ion-icon name="logo-usd" item-end class="dollars-logo"></ion-icon>\n    </ion-item>\n    <button ion-button class="submit-btn" (click)="addAmount()">Submit</button>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/finance_calculator/src/pages/add-expense-popover/add-expense-popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_expenses_expenses__["a" /* ExpensesProvider */]])
    ], AddExpensePopoverPage);
    return AddExpensePopoverPage;
}());

//# sourceMappingURL=add-expense-popover.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpenseReportPageModule", function() { return ExpenseReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expense_report__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ExpenseReportPageModule = /** @class */ (function () {
    function ExpenseReportPageModule() {
    }
    ExpenseReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expense_report__["a" /* ExpenseReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__expense_report__["a" /* ExpenseReportPage */]),
            ],
        })
    ], ExpenseReportPageModule);
    return ExpenseReportPageModule;
}());

//# sourceMappingURL=expense-report.module.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpenseReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_expenses_expenses__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ExpenseReportPage = /** @class */ (function () {
    function ExpenseReportPage(navCtrl, expenses) {
        this.navCtrl = navCtrl;
        this.expenses = expenses;
    }
    Object.defineProperty(ExpenseReportPage.prototype, "annualBills", {
        get: function () {
            return this.addSign(this.expenses.annualBills());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "monthlyBills", {
        get: function () {
            return this.addSign(this.expenses.monthlyBills());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "weeklyBills", {
        get: function () {
            return this.addSign(this.expenses.weeklyBills());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "dailyBills", {
        get: function () {
            return this.addSign(this.expenses.dailyBills());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "oneTimeBills", {
        get: function () {
            return this.addSign(this.expenses.oneTimeBills());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "annualNet", {
        get: function () {
            return this.addSign(this.expenses.annualNet());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "monthlyNet", {
        get: function () {
            return this.addSign(this.expenses.monthlyNet());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "weeklyNet", {
        get: function () {
            return this.addSign(this.expenses.weeklyNet());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "dailyNet", {
        get: function () {
            return this.addSign(this.expenses.dailyNet());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "endDate", {
        get: function () {
            return localStorage.getItem('je_ex_end-date');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "startDate", {
        get: function () {
            return localStorage.getItem('je_ex_start-date');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "totalSavings", {
        get: function () {
            return this.expenses.getTotalSavings();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExpenseReportPage.prototype, "specifiedDuration", {
        get: function () {
            return this.expenses.getSpecifiedDuration();
        },
        enumerable: true,
        configurable: true
    });
    ExpenseReportPage.prototype.addSign = function (value) {
        var num = Number(value);
        if (num > 0) {
            return '+' + value;
        }
        else {
            return value;
        }
    };
    ExpenseReportPage.prototype.updateEndDate = function (event) {
        this.expenses.updateDates(event, 'je_ex_end-date');
    };
    ExpenseReportPage.prototype.updateStartDate = function (event) {
        this.expenses.updateDates(event, 'je_ex_start-date');
    };
    ExpenseReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-expense-report',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/finance_calculator/src/pages/expense-report/expense-report.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Expense Report</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list class="expenses-list">\n    <ion-item>\n        Daily\n    </ion-item>\n      <div class="sub-items-div">\n          <ion-item class="sub-item">\n              Flux: {{dailyBills}}$\n          </ion-item>\n          <ion-item class="sub-item">\n              Net: {{dailyNet}}$\n          </ion-item>\n      </div>\n    <ion-item>\n        Weekly\n    </ion-item>\n      <div class="sub-items-div">\n          <ion-item class="sub-item">\n              Flux: {{weeklyBills}}$\n          </ion-item>\n          <ion-item class="sub-item">\n              Net: {{weeklyNet}}$\n          </ion-item>\n      </div>\n    <ion-item>\n        Monthly\n    </ion-item>\n      <div class="sub-items-div">\n          <ion-item class="sub-item">\n              Flux: {{monthlyBills}}$\n          </ion-item>\n          <ion-item class="sub-item">\n              Net: {{monthlyNet}}$\n          </ion-item>\n      </div>\n    <ion-item>\n        Yearly:\n    </ion-item>\n      <div class="sub-items-div">\n          <ion-item class="sub-item">\n              Flux: {{annualBills}}$\n          </ion-item>\n          <ion-item class="sub-item">\n              Net: {{annualNet}}$\n          </ion-item>\n      </div>\n      <ion-item>\n          One-Time: {{oneTimeBills}}$\n      </ion-item>\n    <ion-item class="total-savings">\n        Total Savings: {{totalSavings}}$\n    </ion-item>\n      <ion-item class="total-savings">\n          <ion-label>Start Date: </ion-label>\n          <ion-datetime displayFormat="MMM DD YYYY" [placeholder]="startDate" [max]="2030" (ionChange)="updateStartDate($event)"></ion-datetime>\n      </ion-item>\n    <ion-item class="total-savings">\n        <ion-label>End Date: </ion-label>\n        <ion-datetime displayFormat="MMM DD YYYY" [placeholder]="endDate" [max]="2040" (ionChange)="updateEndDate($event)"></ion-datetime>\n    </ion-item>\n      <ion-item class="specified-dur">\n          {{specifiedDuration}}\n      </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/finance_calculator/src/pages/expense-report/expense-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_expenses_expenses__["a" /* ExpensesProvider */]])
    ], ExpenseReportPage);
    return ExpenseReportPage;
}());

//# sourceMappingURL=expense-report.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_expense_popover_add_expense_popover__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_expenses_expenses__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__expense_report_expense_report__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(popoverCtrl, expensesProvider, navCtrl, alertCtrl) {
        this.popoverCtrl = popoverCtrl;
        this.expensesProvider = expensesProvider;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
    }
    HomePage.prototype.addExpense = function () {
        var pop = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_2__add_expense_popover_add_expense_popover__["a" /* AddExpensePopoverPage */], {}, { cssClass: 'add-expense-popover' });
        pop.present();
    };
    HomePage.prototype.clearExpenses = function () {
        var _this = this;
        this.alertCtrl.create({
            title: 'Clear',
            message: 'Are you sure you want to clear all expenses? This can not be undone.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Erase',
                    handler: function () {
                        _this.expensesProvider.clearExpenses();
                    }
                }
            ]
        }).present();
    };
    HomePage.prototype.deleteExpense = function (exp) {
        this.expensesProvider.removeExpense(exp.id);
    };
    HomePage.prototype.viewFullExpenseReport = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__expense_report_expense_report__["a" /* ExpenseReportPage */]);
    };
    HomePage.prototype.getExpFrequency = function (frequency) {
        switch (frequency) {
            case 'one-time':
                return 'OT';
            case 'daily':
                return 'D';
            case 'weekly':
                return 'W';
            case 'monthly':
                return 'M';
            case 'annual':
                return 'A';
            default:
                return '?';
        }
    };
    HomePage.prototype.getIncrememntSymbol = function (increment) {
        switch (increment) {
            case 'subtract':
                return '-';
            case 'deposit':
                return '+';
            default:
                return '-';
        }
    };
    Object.defineProperty(HomePage.prototype, "expenses", {
        get: function () {
            var arr = JSON.parse(localStorage.getItem('je_ex_expenses'));
            return arr.reverse();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HomePage.prototype, "endDate", {
        get: function () {
            return localStorage.getItem('je_ex_end-date');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HomePage.prototype, "totalSavings", {
        get: function () {
            return this.expensesProvider.getTotalSavings();
        },
        enumerable: true,
        configurable: true
    });
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/finance_calculator/src/pages/home/home.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-buttons left (click)="clearExpenses()" style="padding-left: 15px;">\n      <button ion-button icon-only class="clear-btn">Clear</button>\n    </ion-buttons>\n    <div class="title">\n      Expenses\n    </div>\n    <ion-buttons right (click)="addExpense()">\n      <button ion-button icon-only class="add-btn"><ion-icon name="add"></ion-icon></button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        End of Year: {{endDate}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-12>\n        Savings: {{totalSavings}}\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-12>\n        <a (click)="viewFullExpenseReport()">View Full Expense Report</a>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-list class="expenses-list">\n    <ion-item-sliding *ngFor="let exp of expenses">\n      <ion-item>\n        <ion-row>\n          <ion-col col-8 class="exp-title-col">\n            {{exp.title}}\n          </ion-col>\n          <ion-col col-4 class="exp-amount-col">\n           {{getIncrememntSymbol(exp.increment)}} ${{exp.amount}} - {{getExpFrequency(exp.frequency)}}\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <ion-item-options side="right">\n        <button ion-button class="delete-option" color="danger" (click)="deleteExpense(exp)">\n          Delete\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/finance_calculator/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_3__providers_expenses_expenses__["a" /* ExpensesProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(222);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_add_expense_popover_add_expense_popover_module__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_expenses_expenses__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_expense_report_expense_report_module__ = __webpack_require__(153);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/add-expense-popover/add-expense-popover.module#AddExpensePopoverPageModule', name: 'AddExpensePopoverPage', segment: 'add-expense-popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expense-report/expense-report.module#ExpenseReportPageModule', name: 'ExpenseReportPage', segment: 'expense-report', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_7__pages_add_expense_popover_add_expense_popover_module__["AddExpensePopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_9__pages_expense_report_expense_report_module__["ExpenseReportPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__providers_expenses_expenses__["a" /* ExpensesProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return short_months; });
var short_months = {
    '1': 'Jan',
    '2': 'Feb',
    '3': 'Mar',
    '4': 'Apr',
    '5': 'May',
    '6': 'Jun',
    '7': 'Jul',
    '8': 'Aug',
    '9': 'Sep',
    '10': 'Oct',
    '11': 'Nov',
    '12': 'Dec'
};
//# sourceMappingURL=months.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            if (!JSON.parse(localStorage.getItem('je_ex_expenses'))) {
                localStorage.setItem('je_ex_expenses', JSON.stringify([]));
            }
            if (!localStorage.getItem('je_ex_end-date')) {
                var date = new Date('January 1 ' + (new Date().getFullYear() + 1)).toDateString();
                date = date.substr(4, date.length);
                localStorage.setItem('je_ex_end-date', date);
            }
            if (!localStorage.getItem('je_ex_start-date')) {
                var date = new Date().toDateString();
                date = date.substr(4, date.length);
                localStorage.setItem('je_ex_start-date', date);
            }
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/jonahelbaz/Desktop/finance_calculator/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/jonahelbaz/Desktop/finance_calculator/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpensesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_months__ = __webpack_require__(248);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExpensesProvider = /** @class */ (function () {
    function ExpensesProvider() {
        this.totalSavings = '0.00';
        this.totalAnnualBills = '0.00';
        this.totalDailyBills = '0.00';
        this.totalOneTimeBills = '0.00';
        this.totalMonthlyBills = '0.00';
        this.totalWeeklyBills = '0.00';
        this.totalAnnualNet = '0.00';
        this.totalMonthlyNet = '0.00';
        this.totalWeeklyNet = '0.00';
        this.totalDailyNet = '0.00';
        this.recalculateSavings();
    }
    ExpensesProvider.prototype.updateDates = function (event, key) {
        var month = __WEBPACK_IMPORTED_MODULE_1__config_months__["a" /* short_months */][event.month.toString()];
        var res = month + ' ' + event.day + ' ' + event.year;
        localStorage.setItem(key, res);
        this.recalculateSavings();
    };
    ExpensesProvider.prototype.getTotalSavings = function () {
        return this.totalSavings;
    };
    ExpensesProvider.prototype.annualBills = function () {
        return this.totalAnnualBills;
    };
    ExpensesProvider.prototype.monthlyBills = function () {
        return this.totalMonthlyBills;
    };
    ExpensesProvider.prototype.weeklyBills = function () {
        return this.totalWeeklyBills;
    };
    ExpensesProvider.prototype.dailyBills = function () {
        return this.totalDailyBills;
    };
    ExpensesProvider.prototype.oneTimeBills = function () {
        return this.totalOneTimeBills;
    };
    ExpensesProvider.prototype.annualNet = function () {
        return this.totalAnnualNet;
    };
    ExpensesProvider.prototype.monthlyNet = function () {
        return this.totalMonthlyNet;
    };
    ExpensesProvider.prototype.weeklyNet = function () {
        return this.totalWeeklyNet;
    };
    ExpensesProvider.prototype.dailyNet = function () {
        return this.totalDailyNet;
    };
    ExpensesProvider.prototype.addNewExpense = function (title, amount, selected, increment) {
        var expenses = JSON.parse(localStorage.getItem('je_ex_expenses'));
        expenses.push({
            title: title,
            amount: amount,
            id: new Date().getTime().toString(),
            frequency: selected,
            increment: increment
        });
        localStorage.setItem('je_ex_expenses', JSON.stringify(expenses));
        this.recalculateSavings();
    };
    ExpensesProvider.prototype.removeExpense = function (id) {
        var expenses = JSON.parse(localStorage.getItem('je_ex_expenses'));
        var i = expenses.findIndex(function (o) { return o.id === id; });
        if (i > -1) {
            expenses.splice(i, 1);
        }
        localStorage.setItem('je_ex_expenses', JSON.stringify(expenses));
        this.recalculateSavings();
    };
    ExpensesProvider.prototype.clearExpenses = function () {
        localStorage.setItem('je_ex_expenses', JSON.stringify([]));
        this.totalSavings = '0.00';
        this.totalAnnualBills = '0.00';
        this.totalDailyBills = '0.00';
        this.totalOneTimeBills = '0.00';
        this.totalMonthlyBills = '0.00';
        this.totalWeeklyBills = '0.00';
        this.totalAnnualNet = '0.00';
        this.totalMonthlyNet = '0.00';
        this.totalWeeklyNet = '0.00';
        this.totalDailyNet = '0.00';
    };
    ExpensesProvider.prototype.getSpecifiedDuration = function () {
        var endDate = localStorage.getItem('je_ex_end-date');
        var startDate = localStorage.getItem('je_ex_start-date');
        var endDateObj = new Date(endDate);
        var startDateObj = new Date(startDate);
        var diff = Math.floor((endDateObj.getTime() - startDateObj.getTime()) / 1000);
        var years = diff / 3.154e+7;
        var yearsRm = years.toString().substr(years.toString().indexOf('.'), years.toString().length);
        var months = Number(yearsRm) * 12;
        var monthsRm = months.toString().substr(months.toString().indexOf('.'), months.toString().length);
        var days = Math.floor(Number(monthsRm) * 31);
        var res = '';
        Math.floor(years) > 0 ? res += Math.floor(years) + ' Year' + (Math.floor(years) > 1 ? 's' : '') : res;
        Math.floor(months) > 0 ? res += ', ' + Math.floor(months) + ' Month' + (Math.floor(months) > 1 ? 's' : '') : res;
        Math.floor(days) > 0 ? res += ', ' + Math.floor(days) + ' Day' + (Math.floor(days) > 1 ? 's' : '') : res;
        if (res.substr(0, 1) === ',') {
            return res.substr(1, res.length);
        }
        return res;
    };
    ExpensesProvider.prototype.recalculateSavings = function () {
        var endDate = localStorage.getItem('je_ex_end-date');
        var startDate = localStorage.getItem('je_ex_start-date');
        var endDateObj = new Date(endDate);
        var startDateObj = new Date(startDate);
        var breakdown = this.getDateBreakdown(endDateObj, startDateObj);
        this.calculateTotalBills(breakdown);
        this.calculateTotalNet(breakdown);
    };
    ExpensesProvider.prototype.getDateBreakdown = function (endDate, currentDate) {
        var diff = (endDate.getTime() - currentDate.getTime()) / 1000;
        return {
            daily: diff / (3600 * 24),
            weekly: diff / 604800,
            monthly: diff / 2.628e+6,
            annual: diff / 3.154e+7
        };
    };
    ExpensesProvider.prototype.calculateTotalBills = function (breakDown) {
        var expenses = JSON.parse(localStorage.getItem('je_ex_expenses'));
        var daily = this.calculateExpenses(expenses, 'daily');
        var weekly = this.calculateExpenses(expenses, 'weekly');
        var monthly = this.calculateExpenses(expenses, 'monthly');
        var annual = this.calculateExpenses(expenses, 'annual');
        var oneTime = this.calculateOneTimeExpenses(expenses);
        this.totalSavings = (Math.round(((daily * breakDown.daily) + (weekly * breakDown.weekly) + (monthly * breakDown.monthly) + (annual * breakDown.annual) + oneTime) * 100) / 100).toString();
        this.totalDailyBills = (Math.round(daily * 100) / 100).toString();
        this.totalWeeklyBills = (Math.round(weekly * 100) / 100).toString();
        this.totalMonthlyBills = (Math.round(monthly * 100) / 100).toString();
        this.totalAnnualBills = (Math.round(annual * 100) / 100).toString();
        this.totalOneTimeBills = (Math.round(oneTime * 100) / 100).toString();
    };
    ExpensesProvider.prototype.calculateTotalNet = function (breakDown) {
        this.totalDailyNet = (Math.round((Number(this.totalSavings) / breakDown.daily) * 100) / 100).toString();
        this.totalWeeklyNet = (Math.round((Number(this.totalSavings) / breakDown.weekly) * 100) / 100).toString();
        this.totalMonthlyNet = (Math.round((Number(this.totalSavings) / breakDown.monthly) * 100) / 100).toString();
        this.totalAnnualNet = (Math.round((Number(this.totalSavings) / breakDown.annual) * 100) / 100).toString();
    };
    ExpensesProvider.prototype.calculateExpenses = function (expenses, prop) {
        var amnt = 0;
        expenses.forEach(function (exp) {
            if (exp.frequency === prop) {
                exp.increment === 'deposit' ?
                    amnt += exp.amount :
                    amnt -= exp.amount;
            }
        });
        return amnt;
    };
    ExpensesProvider.prototype.calculateOneTimeExpenses = function (expenses) {
        var amnt = 0;
        expenses.forEach(function (exp) {
            if (exp.frequency === 'one-time') {
                exp.increment === 'deposit' ?
                    amnt += exp.amount :
                    amnt -= exp.amount;
            }
        });
        return amnt;
    };
    ExpensesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ExpensesProvider);
    return ExpensesProvider;
}());

//# sourceMappingURL=expenses.js.map

/***/ })

},[199]);
//# sourceMappingURL=main.js.map