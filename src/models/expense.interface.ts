export interface Expense {
    title: string;
    amount: number;
    id: string;
    frequency: string;
    increment: string;
}