import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      if (!JSON.parse(localStorage.getItem('je_ex_expenses'))) {
          localStorage.setItem('je_ex_expenses', JSON.stringify([]));
      }
      if (!localStorage.getItem('je_ex_end-date')) {
        let date = new Date('January 1 ' + (new Date().getFullYear() + 1)).toDateString();
        date = date.substr(4, date.length);
        localStorage.setItem('je_ex_end-date', date);
      }
      if (!localStorage.getItem('je_ex_start-date')) {
          let date = new Date().toDateString();
          date = date.substr(4, date.length);
          localStorage.setItem('je_ex_start-date', date);
      }
    });
  }
}

