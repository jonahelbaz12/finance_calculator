import {Injectable} from '@angular/core';
import {Expense} from '../../models/expense.interface';
import {short_months} from '../../config/months';

@Injectable()
export class ExpensesProvider {
  private totalSavings = '0.00';
  private totalAnnualBills = '0.00';
  private totalDailyBills = '0.00';
  private totalOneTimeBills = '0.00';
  private totalMonthlyBills = '0.00';
  private totalWeeklyBills = '0.00';
  private totalAnnualNet = '0.00';
  private totalMonthlyNet = '0.00';
  private totalWeeklyNet = '0.00';
  private totalDailyNet = '0.00';
  constructor() {
      this.recalculateSavings();
  }

  public updateDates(event: any, key: string): void {
      const month = short_months[event.month.toString()];
      const res = month + ' ' + event.day + ' ' + event.year;
      localStorage.setItem(key, res);
      this.recalculateSavings();
  }

  public getTotalSavings(): string {
    return this.totalSavings;
  }

  public annualBills(): string {
       return this.totalAnnualBills;
    }

  public monthlyBills(): string {
        return this.totalMonthlyBills;
    }

  public weeklyBills(): string {
        return this.totalWeeklyBills;
    }

  public dailyBills(): string {
        return this.totalDailyBills;
    }

  public oneTimeBills(): string {
        return this.totalOneTimeBills;
    }

  public annualNet(): string {
        return this.totalAnnualNet;
    }

  public monthlyNet(): string {
        return this.totalMonthlyNet;
    }

  public weeklyNet(): string {
        return this.totalWeeklyNet;
    }

  public dailyNet(): string {
        return this.totalDailyNet;
    }

  public addNewExpense(title: string, amount: number, selected: string, increment: string): void {
    const expenses: Expense[] = JSON.parse(localStorage.getItem('je_ex_expenses'));
    expenses.push({
       title: title,
       amount: amount,
       id: new Date().getTime().toString(),
       frequency: selected,
       increment: increment
    });
    localStorage.setItem('je_ex_expenses', JSON.stringify(expenses));
    this.recalculateSavings();
  }

  public removeExpense(id: string): void {
      const expenses: Expense[] = JSON.parse(localStorage.getItem('je_ex_expenses'));
      const i = expenses.findIndex(o => o.id === id);
      if (i > -1) {
        expenses.splice(i, 1);
      }
      localStorage.setItem('je_ex_expenses', JSON.stringify(expenses));
      this.recalculateSavings();
  }

  public clearExpenses(): void {
      localStorage.setItem('je_ex_expenses', JSON.stringify([]));
      this.totalSavings = '0.00';
      this.totalAnnualBills = '0.00';
      this.totalDailyBills = '0.00';
      this.totalOneTimeBills = '0.00';
      this.totalMonthlyBills = '0.00';
      this.totalWeeklyBills = '0.00';
      this.totalAnnualNet = '0.00';
      this.totalMonthlyNet = '0.00';
      this.totalWeeklyNet = '0.00';
      this.totalDailyNet = '0.00';
  }

  public getSpecifiedDuration(): string {
      const endDate = localStorage.getItem('je_ex_end-date');
      const startDate = localStorage.getItem('je_ex_start-date');
      const endDateObj = new Date(endDate);
      const startDateObj = new Date(startDate);
      const diff = Math.floor((endDateObj.getTime() - startDateObj.getTime()) / 1000);
      const years = diff / 3.154e+7;
      const yearsRm = years.toString().substr(years.toString().indexOf('.'), years.toString().length);
      const months = Number(yearsRm) * 12;
      const monthsRm = months.toString().substr(months.toString().indexOf('.'), months.toString().length);
      const days = Math.floor(Number(monthsRm) * 31);
      let res = '';
      Math.floor(years) > 0 ? res += Math.floor(years) + ' Year' + (Math.floor(years) > 1 ? 's' : '') : res;
      Math.floor(months) > 0 ? res += ', ' + Math.floor(months) + ' Month' + (Math.floor(months) > 1 ? 's' : '') : res;
      Math.floor(days) > 0 ? res += ', ' + Math.floor(days) + ' Day' + (Math.floor(days) > 1 ? 's' : '') : res;
      if (res.substr(0, 1) === ',') {
          return res.substr(1, res.length);
      }
      return res;
  }

  private recalculateSavings(): void {
      const endDate = localStorage.getItem('je_ex_end-date');
      const startDate = localStorage.getItem('je_ex_start-date');
      const endDateObj = new Date(endDate);
      const startDateObj = new Date(startDate);
      const breakdown = this.getDateBreakdown(endDateObj, startDateObj);
      this.calculateTotalBills(breakdown);
      this.calculateTotalNet(breakdown);
  }

  private getDateBreakdown(endDate: any, currentDate: any) {
    const diff = (endDate.getTime() - currentDate.getTime()) / 1000;
    return {
       daily: diff / (3600*24),
       weekly: diff / 604800,
       monthly: diff / 2.628e+6,
       annual: diff / 3.154e+7
    }
  }

  private calculateTotalBills(breakDown: any): void {
      const expenses: Expense[] = JSON.parse(localStorage.getItem('je_ex_expenses'));
      const daily: number = this.calculateExpenses(expenses, 'daily');
      const weekly: number = this.calculateExpenses(expenses, 'weekly');
      const monthly: number = this.calculateExpenses(expenses, 'monthly');
      const annual: number = this.calculateExpenses(expenses, 'annual');
      const oneTime: number = this.calculateOneTimeExpenses(expenses);
      this.totalSavings = (Math.round(((daily * breakDown.daily) + (weekly * breakDown.weekly) + (monthly * breakDown.monthly) + (annual * breakDown.annual) + oneTime) * 100) / 100).toString();
      this.totalDailyBills = (Math.round(daily * 100) / 100).toString();
      this.totalWeeklyBills = (Math.round(weekly * 100) / 100).toString();
      this.totalMonthlyBills = (Math.round(monthly * 100) / 100).toString();
      this.totalAnnualBills = (Math.round(annual * 100) / 100).toString();
      this.totalOneTimeBills = (Math.round(oneTime * 100) / 100).toString();
  }

  private calculateTotalNet(breakDown: any) {
      this.totalDailyNet = (Math.round((Number(this.totalSavings) / breakDown.daily) * 100) / 100).toString();
      this.totalWeeklyNet = (Math.round((Number(this.totalSavings) / breakDown.weekly) * 100) / 100).toString();
      this.totalMonthlyNet = (Math.round((Number(this.totalSavings) / breakDown.monthly) * 100) / 100).toString();
      this.totalAnnualNet = (Math.round((Number(this.totalSavings) / breakDown.annual) * 100) / 100).toString();

  }

  private calculateExpenses(expenses: Expense[], prop: string): number {
      let amnt = 0;
      expenses.forEach(exp => {
         if (exp.frequency === prop) {
             exp.increment === 'deposit' ?
                 amnt += exp.amount :
                 amnt -= exp.amount;
         }
      });
      return amnt;
  }

  private calculateOneTimeExpenses(expenses): number {
      let amnt = 0;
      expenses.forEach(exp => {
          if (exp.frequency === 'one-time') {
              exp.increment === 'deposit' ?
                  amnt += exp.amount :
                  amnt -= exp.amount;
          }
      });
      return amnt;
  }
}
