import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {ExpensesProvider} from '../../providers/expenses/expenses';

@IonicPage()
@Component({
  selector: 'page-expense-report',
  templateUrl: 'expense-report.html',
})
export class ExpenseReportPage {
  constructor(private navCtrl: NavController, private expenses: ExpensesProvider) {
  }

  get annualBills(): string {
    return this.addSign(this.expenses.annualBills());
  }

  get monthlyBills(): string {
    return this.addSign(this.expenses.monthlyBills());
  }

  get weeklyBills(): string {
    return this.addSign(this.expenses.weeklyBills());
  }

  get dailyBills(): string {
    return this.addSign(this.expenses.dailyBills());
  }

  get oneTimeBills(): string {
    return this.addSign(this.expenses.oneTimeBills());
  }

  get annualNet(): string {
      return this.addSign(this.expenses.annualNet());
  }

  get monthlyNet(): string {
      return this.addSign(this.expenses.monthlyNet());
  }

  get weeklyNet(): string {
      return this.addSign(this.expenses.weeklyNet());
  }

  get dailyNet(): string {
      return this.addSign(this.expenses.dailyNet());
  }

  get endDate(): string {
      return localStorage.getItem('je_ex_end-date');
  }

  get startDate(): string {
      return localStorage.getItem('je_ex_start-date');
  }

  get totalSavings(): string {
      return this.expenses.getTotalSavings();
  }

  get specifiedDuration(): string {
      return this.expenses.getSpecifiedDuration();
  }

  public addSign(value: string): string {
      const num = Number(value);
      if (num > 0) {
          return '+' + value;
      } else {
          return value;
      }
  }

  public updateEndDate(event: any): void {
      this.expenses.updateDates(event, 'je_ex_end-date');
  }

    public updateStartDate(event: any): void {
        this.expenses.updateDates(event, 'je_ex_start-date');
    }
}
