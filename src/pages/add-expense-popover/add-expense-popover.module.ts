import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddExpensePopoverPage } from './add-expense-popover';

@NgModule({
  declarations: [
    AddExpensePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(AddExpensePopoverPage),
  ],
})
export class AddExpensePopoverPageModule {}
