import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController} from 'ionic-angular';
import {ExpensesProvider} from '../../providers/expenses/expenses';

@IonicPage()
@Component({
  selector: 'page-add-expense-popover',
  templateUrl: 'add-expense-popover.html',
})
export class AddExpensePopoverPage {
  public selected = '';
  public title: string = '';
  public amount: string = '';
  public increment: string  = '';
  constructor(private navCtrl: NavController, private alertCtrl: AlertController,
              private expenses: ExpensesProvider) {
  }

  public close(): void {
    this.navCtrl.pop();
  }

  public addAmount(): void {
      if (!this.amount || !this.title || this.title === '' || this.amount === '' || this.amount === '0' || this.amount === '0.00') return;
      this.amount = this.amount.replace(',', '');
      if (!Number(this.amount)) {
        this.alertCtrl.create({
            title: 'Oops',
            message: 'Amount must be numbers only!',
            buttons: ['OK']
        }).present();
        return;
      }
      if (this.selected === '' || this.increment === '') {
          this.alertCtrl.create({
              title: 'Oops',
              message: 'Please select a frequency and increment!',
              buttons: ['OK']
          }).present();
          return;
      }
      this.expenses.addNewExpense(this.title, Number(this.amount), this.selected, this.increment);
      this.navCtrl.pop();
  }

}
