import { Component } from '@angular/core';
import {AlertController, NavController, PopoverController} from 'ionic-angular';
import {AddExpensePopoverPage} from '../add-expense-popover/add-expense-popover';
import {Expense} from '../../models/expense.interface';
import {ExpensesProvider} from '../../providers/expenses/expenses';
import {ExpenseReportPage} from '../expense-report/expense-report';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private popoverCtrl: PopoverController, private expensesProvider: ExpensesProvider,
              private navCtrl: NavController, private alertCtrl: AlertController) { }

  public addExpense(): void {
      const pop = this.popoverCtrl.create(AddExpensePopoverPage, {}, {cssClass: 'add-expense-popover'});
      pop.present();
  }

  public clearExpenses(): void {
      this.alertCtrl.create({
          title: 'Clear',
          message: 'Are you sure you want to clear all expenses? This can not be undone.',
          buttons: [
              {
                  text: 'Cancel',
                  role: 'cancel'
              },
              {
                  text: 'Erase',
                  handler: () => {
                      this.expensesProvider.clearExpenses();
                  }
              }

          ]
      }).present();
  }

  public deleteExpense(exp: Expense): void {
      this.expensesProvider.removeExpense(exp.id);
  }

  public viewFullExpenseReport(): void {
      this.navCtrl.push(ExpenseReportPage);
  }

  public getExpFrequency(frequency: string): string {
      switch(frequency) {
          case 'one-time':
              return 'OT';
          case 'daily':
              return 'D';
          case 'weekly':
              return 'W';
          case 'monthly':
              return 'M';
          case 'annual':
              return 'A';
          default:
              return '?';
      }
  }

  public getIncrememntSymbol(increment: string): string {
      switch(increment) {
          case 'subtract':
              return '-';
          case 'deposit':
              return '+';
          default:
              return '-';
      }
  }

  get expenses(): Expense[] {
      const arr: Expense[] = JSON.parse(localStorage.getItem('je_ex_expenses'));
      return arr.reverse();
  }

  get endDate(): string {
      return localStorage.getItem('je_ex_end-date');
  }

  get totalSavings(): string {
      return this.expensesProvider.getTotalSavings();
  }

}
